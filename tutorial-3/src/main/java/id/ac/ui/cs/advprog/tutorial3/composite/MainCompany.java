package id.ac.ui.cs.advprog.tutorial3.composite;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.*;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

public class MainCompany {
	public static void main(String[] args){
		Company company = new Company();
		Employees ceo = new Ceo("Faza", 300000.00);
		company.addEmployee(ceo);
		Employees cto = new Cto("Darina", 150000.00);
		company.addEmployee(cto);
		
		Employees backendProgrammer = new BackendProgrammer("A", 25000.00);
		company.addEmployee(backendProgrammer);
		Employees frontendProgrammer = new FrontendProgrammer("B", 32000.00);
		company.addEmployee(frontendProgrammer);
		Employees networkExpert = new NetworkExpert("C", 50000.00);
		company.addEmployee(networkExpert);
		Employees securityExpert = new SecurityExpert("D", 75000.00);
		company.addEmployee(securityExpert);
		Employees uiuxDesigner = new UiUxDesigner("E", 98000.00);
		company.addEmployee(uiuxDesigner);
		
		System.out.println("All Employees");
		for(Employees employee : company.getAllEmployees()){
			System.out.printf("%8s %20s %8f %n", employee.getName(), employee.getRole(), employee.getSalary());
		}
		System.out.println("Net Salaries : " + company.getNetSalaries());
	}
}
