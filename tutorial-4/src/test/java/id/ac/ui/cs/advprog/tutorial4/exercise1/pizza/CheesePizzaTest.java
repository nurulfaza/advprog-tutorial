package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CheesePizzaTest {
	private Class<?> cheesePizzaClass;
	
	@Before
	public void setUp() throws ClassNotFoundException {
		cheesePizzaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.CheesePizza");
	}
	
	@Test
	public void testCheesePizzaIsAPizza() {
		Class<?> parent = cheesePizzaClass.getSuperclass();
		
		assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza", parent.getName());
	}
}