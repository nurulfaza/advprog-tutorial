package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ZucchiniTest {
	private Zucchini zucchini;
	
	@Before
	public void setUp() {
		zucchini = new Zucchini();
	}
	
	@Test
	public void testMethodToString() {
		assertEquals("Zucchini", zucchini.toString());
	}
}
