package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Before;
import org.junit.Test;

public class PizzaTest {
	private Class<?> pizzaClass;
	
	@Before
    public void setUp() throws Exception {
        pizzaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza");
    }

    @Test
    public void testPizzaIsAbstract() {
        int classModifiers = pizzaClass.getModifiers();

        assertTrue(Modifier.isAbstract(classModifiers));
    }

    @Test
    public void testPizzaHasPrepareMethod() throws Exception {
        Method display = pizzaClass.getDeclaredMethod("prepare");
        int methodModifiers = display.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals("void", display.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPizzaHasBakeMethod() throws Exception {
        Method display = pizzaClass.getDeclaredMethod("bake");
        int methodModifiers = display.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("void", display.getGenericReturnType().getTypeName());
    }
    
    @Test
    public void testPizzaHasCutMethod() throws Exception {
        Method display = pizzaClass.getDeclaredMethod("cut");
        int methodModifiers = display.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("void", display.getGenericReturnType().getTypeName());
    }
    
    @Test
    public void testPizzaHasBoxMethod() throws Exception {
        Method display = pizzaClass.getDeclaredMethod("box");
        int methodModifiers = display.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("void", display.getGenericReturnType().getTypeName());
    }
}
