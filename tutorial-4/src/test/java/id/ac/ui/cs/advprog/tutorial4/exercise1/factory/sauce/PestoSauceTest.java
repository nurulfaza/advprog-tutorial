package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class PestoSauceTest {
private PestoSauce pestoSauce;
	
	@Before
	public void setUp() {
		pestoSauce = new PestoSauce();
	}
	
	@Test
	public void testMethodToString() {
		assertEquals("Pesto Sauce", pestoSauce.toString());
	}
}
