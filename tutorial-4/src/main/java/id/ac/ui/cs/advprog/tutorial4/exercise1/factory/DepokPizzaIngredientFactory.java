package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.CheddarCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.SoftShellClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.StuffedCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PestoSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Zucchini;


public class DepokPizzaIngredientFactory implements PizzaIngredientFactory{
	@Override
	public Dough createDough() {
		return new StuffedCrustDough();
	}

	@Override
	public Sauce createSauce() {
		return new PestoSauce();
	}

	@Override
	public Cheese createCheese() {
		return new CheddarCheese();
	}

	@Override
	public Veggies[] createVeggies() {
		Veggies[] veggies = {new Zucchini(), new Spinach(), new Mushroom(), new Onion()};
		return veggies;
	}

	@Override
	public Clams createClam() {
		// TODO Auto-generated method stub
		return new SoftShellClams();
	}

}
