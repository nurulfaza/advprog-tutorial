package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = true)
                                       String name, Model model) {
        //model.addAttribute("name", name);
        if (name == null || name.equals("")){
            model.addAttribute("title", "This is my CV");
        }
        else {
            model.addAttribute("title", name + ", I hope you interested to hire me.");
        }
        
        StringBuilder cv = new StringBuilder();
        cv.append("Full Name : Nurul Faza Saffanah\n");
        cv.append("Birthdate : April 7th 1998\n");
        cv.append("Birthplace : Jakarta\n");
        cv.append("Education History :\n");
        cv.append("  - Universitas Indonesia (2016-present)\n");
        cv.append("  - SMA Islam Al-Azhar 1 (2013-2016)\n");
        cv.append("  - SMP Islam Al-Azhar 1 (2010-2013)\n");
        cv.append("  - SD Islam Al-Azhar 1 (2004-2010)\n");
        
        model.addAttribute("cv", cv.toString());
        
        StringBuilder desc = new StringBuilder();
        desc.append("Hi my name is Nurul Faza Saffanah, people usually called me Faza.\n");
        desc.append("I'm currently studying at Universitas Indonesia as an undergraduate \n"
            + "student majoring in Computer Science. My hobby is listening to   \n"
            + "good songs. My favorite music genre is RnB and Pop. I live in Jakarta Selatan, "
            + "and I don't have a 'kost',\n"
   	        + "so I'm considered as a PP person. I'm using Commuter as transportation everyday\n"
            + "to go to my university.");
        
        model.addAttribute("desc", desc.toString());
        
        return "greeting";
    }

}
