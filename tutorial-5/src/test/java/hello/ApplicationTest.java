/*
 * Copyright 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package hello;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = GreetingController.class)
public class ApplicationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void homePage() throws Exception {
        // N.B. jsoup can be useful for asserting HTML content
        mockMvc.perform(get("/index.html"))
                .andExpect(content().string(containsString("Get your greeting")));
    }

    @Test
    public void greetingWithUser() throws Exception {
        mockMvc.perform(get("/greeting").param("name", "Faza"))
                .andExpect(content().string(containsString("Faza, I hope you "
                        + "interested to hire me.")));
        mockMvc.perform(get("/greeting").param("name", ""))
                .andExpect(content().string(containsString("This is my CV")));
              
        /**mockMvc.perform(get("/greeting").param("name", "Faza"))
                .andExpect(content().string(containsString("Full Name : 
                Nurul Faza Saffanah Birthdate : April 7th 1998 Birthplace : 
                Jakarta Education History : - Universitas Indonesia (2016-present) 
                - SMA Islam Al-Azhar 1 (2013-2016) - SMP Islam Al-Azhar 1 (2010-2013) 
                - SD Islam Al-Azhar 1 (2004-2010)")));
        mockMvc.perform(get("/greeting").param("name", "Faza"))
                .andExpect(content().string(containsString("Hi my name is Nurul Faza Saffanah, 
                people usually called me Faza.\nI'm currently studying at 
                Universitas Indonesia as an undergraduate \n"
                +   "student majoring in Computer Science. My hobby is listening to   \n"
                +   "good songs. My favorite music genre is RnB and Pop. I live 
                in Jakarta Selatan, and I don't have a 'kost',   \n"
                +   "so I'm considered as a PP person. I'm using Commuter as transportation 
                everyday\n"
                +   "to go to my university."))); **/
    }

}
